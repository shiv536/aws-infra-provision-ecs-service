output "elb_dns_name" {
  description = "The DNS name of the ELB"
  value       = aws_alb.application_load_balancer.dns_name
}
