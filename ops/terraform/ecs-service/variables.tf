variable "vpc" {
default = "MyVPC"
}

variable "subnet_string" {
default = "Public"
}

variable "security_group_id" {
default = "sg-0b4ec4f603bad7a81"
}

variable "lb_tg_name" {
    default = "ecs-Fargate-Service"
}


