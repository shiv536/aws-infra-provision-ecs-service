provider "aws" {
  region = "us-east-1"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  backend "s3" {
    region = "us-east-1"
    bucket = "terraform-tf-state-001"
    key = "ecs-service/terraform.tfstate"
    encrypt = true
  }
}

data "aws_lb_target_group" "target_group" {
  name = "${var.lb_tg_name}"
}

data "aws_vpc" "main" {
  filter {
    name   = "tag:Name"
    values = ["${var.vpc}"]
  }
}

data "aws_subnets" "public_app_subnets" {
   filter {
    name   = "tag:Name"
    values = ["*${var.subnet_string}-*"]
  }
}

resource "aws_cloudwatch_log_group" "ecs_log" {
  name = "ecs_service"
  retention_in_days = 7

  tags = {
    Environment = "dev"
    Application = "apache"
  }
}

resource "aws_ecs_cluster" "my_cluster" {
  name = "FargateCluster" # Naming the cluster
}

resource "aws_ecs_task_definition" "my_first_task" {
  family                   = "web-task-001"
  container_definitions    = <<DEFINITION
  [
    {
      "name": "web-task-001",
      "image": "196575384030.dkr.ecr.us-east-1.amazonaws.com/web-service-repo:dev",
      "essential": true,
      "links": [],
      "privileged": false, 
      "portMappings": [
        {
          "containerPort": 8080,
          "hostPort": 8080,
          "protocol": "TCP"
        }
      ],
      "memory": 1024,
      "cpu": 200,
      "memoryReservation": 1024,
     "logConfiguration": {
       "logDriver": "awslogs",
       "options": {
	  "awslogs-group": "ecs_service",
          "awslogs-region": "us-east-1",
	  "awslogs-stream-prefix": "ecs_service"
	}
      }
    }
  ]
  DEFINITION
  requires_compatibilities = ["FARGATE"] # Stating that we are using ECS Fargate
  network_mode             = "awsvpc"    # Using awsvpc as our network mode as this is required for Fargate
  memory                   = 1024         # Specifying the memory our container requires
  cpu                      = 512         # Specifying the CPU our container requires
  execution_role_arn       = "arn:aws:iam::196575384030:role/ecsTaskExecutionRole"
}


data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com","ecs.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecsTaskRole" {
  name                = "ecsTaskExecutionRole"
  assume_role_policy  = data.aws_iam_policy_document.assume_role_policy.json
  managed_policy_arns = ["arn:aws:iam::196575384030:policy/service_execution_policy"]
}

resource "aws_iam_role_policy_attachment" "policy" {
  role       = aws_iam_role.ecsTaskRole.name
  policy_arn = "arn:aws:iam::196575384030:policy/service_execution_policy"
}

resource "aws_ecs_service" "service" {
  name                               = "WebService"                            # Naming our first service
  cluster                            = aws_ecs_cluster.my_cluster.id             # Referencing our created Cluster
  task_definition                    = aws_ecs_task_definition.my_first_task.arn # Referencing the task our service will spin up
  launch_type                        = "FARGATE"
  desired_count                      = 4
  #wait_for_steady_state             = true
  deployment_maximum_percent         = 150
  deployment_minimum_healthy_percent = 50
  enable_ecs_managed_tags            = true
  #health_check_grace_period_seconds = 3

  network_configuration {
    subnets          = "${data.aws_subnets.public_app_subnets.ids}"
    security_groups = ["${var.security_group_id}"]
    assign_public_ip = true # Providing our containers with public IPs
  }
  load_balancer {
    target_group_arn = "${data.aws_lb_target_group.target_group.arn}"
    container_name   = "web-task-001"
    container_port   = 8080 # Specifying the container port
  }
}
