provider "aws" {
  region = "us-east-1"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  backend "s3" {
    region = "us-east-1"
    bucket = "terraform-tf-state-001"
    key = "alb-tg/terraform.tfstate"
    encrypt = true
  }
}

data "aws_security_group" "selected" {
  id = var.security_group_id
}

data "aws_vpc" "main" {
  filter {
    name   = "tag:Name"
    values = ["${var.vpc}"]
  }
}

data "aws_subnets" "public_app_subnets" {
   filter {
    name   = "tag:Name"
    values = ["*${var.subnet_string}-*"]
  }
}


resource "aws_alb" "application_load_balancer" {
  name               = "ALB-1" # Naming our load balancer
  load_balancer_type = "application"
  subnets = "${data.aws_subnets.public_app_subnets.ids}"
  # Referencing the security group
  security_groups = ["${data.aws_security_group.selected.id}"]
}
resource "aws_lb_target_group" "target_group" {
  name        = "ecs-Fargate-Service"
  port        = 8080
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = "${data.aws_vpc.main.id}"
 health_check {
    path                = "/"
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    interval            = 300
    matcher             = "200,301,302"
  }
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_alb.application_load_balancer.arn
  port              = "8080"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
}
