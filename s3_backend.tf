terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.11"
    }
  }
  backend "local" {
    path = "relative/path/to/terraform.tfstate"
  }
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-tf-state-001"
}
