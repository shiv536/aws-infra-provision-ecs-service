provider "aws" {
  region = "us-east-1"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  backend "s3" {
    region = "us-east-1"
    bucket = "terraform-tf-state-001"
    key = "ecs/terraform.tfstate"
    encrypt = true
  }
}

resource "aws_ecr_repository" "ecs_repo" {
  name = "web-service-repo" 
}
